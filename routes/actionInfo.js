
var timeFormater = require('./timeFormat').getCurrentTime;
var _                  = require('underscore');
var Q                  = require('q');


// { epc: '101', itemType: '01', collection: 'epc'}
// {productCode:"01",productName:"电脑", collection: 'itemType'}


function actionInfo(_action, _note){
	this.actionID = _action;
	this.timeStamp = timeFormater();
	if(_note == null){
		this.note = "";
	}else
		this.note = _note;
}
actionInfo.prototype.tryValidate = function() {
	if(this.actionID == null || this.actionID.length <=0){
		console.log(('actionInfo tryValidate error').error);
		return false;
	}else{
		return true;
	} 
};

exports.actionList = function(req, res){
	var actionID = req.body.actionID;
	actionDBFind({actionID: actionID})
	.then(function(_list){
		var str = JSON.stringify(_list);
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log(('error <= actionlist' + error.message).error);
		res.send(error.message);
	})
};
exports.addAction = function(req, res){
	var body = req.body;
	var actionID = body.actionID;
	var note = body.note;

	_addAction(actionID, note).then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log(('error <= addAction' + error.message).error);
		res.send(error.message);
	})
}
exports.removeAction = function(req, res){
	var actionID = req.body.actionID;
	actionDBRemove({actionID: actionID}).then(function(_number){
		if(_number <= 0){
			res.send('noData');
		}else{
			res.send('ok');
		}
	}).catch(function(error){
		console.log(('error <= removeAction ' + error.message).error);
		res.send('error');
	})
}
exports.testAddAction = function(req, res){
	_addAction('000001', 'inventory test')
	.then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log(('error <= testAddAction' + error.message).error);
		res.send('error');
	})
}
function _addAction(_actionID, _note){
	var actionNew = new actionInfo(_actionID, _note);
	// console.dir(actionNew);
	if(actionNew.tryValidate()){
		return actionDBFind({actionID: actionNew.actionID}).then(function(_list){
			if(_.size(_list) > 0){
				return actionDBRemove({actionID: actionNew.actionID})
				.then(function(_number){
					return actionDBInsert(actionNew);
				})
			}else{
				return actionDBInsert(actionNew);
			}
		})
	}else{
		return Q.fcall(function(){
			throw new  Error('paraError');	
		})
	}
}

//****************************************************************************
