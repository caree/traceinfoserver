
var timeFormater = require('./timeFormat').getCurrentTime;
var _                  = require('underscore');
var Q                  = require('q');


function productHistoryKeyPoint(_productEPC, _actionID, _paraID){
	this.productEPC = _productEPC;
	this.actionID = _actionID;
	if(_paraID == null){
		this.paraID = '';
	}else
		this.paraID = _paraID;
	this.timeStamp = timeFormater();
}
productHistoryKeyPoint.prototype.tryValidate = function() {
	if(this.productEPC == null || this.productEPC.length <=0 
		|| this.actionID == null || this.actionID.length <= 0){
		console.log(('productHistoryKeyPoint tryValidate error').error);
		return false;
	}else{
		return true;
	} 
};
exports.productHistoryKeyPointListAll = function(req, res){
	productHistoryKeyPointDBFind({})
	.then(function(_list){
		paraInfoDBFind({}).then(function(_paraList){
			var list = _.map(_list, function(_historyPoint){
				var para = _.findWhere(_paraList, {paraID: _historyPoint.paraID});
				if(para != null){
					_historyPoint.paraName = para.paraName;
					_historyPoint.paraValue = para.paraValue;
				}
				return _historyPoint;
			});
			var str = JSON.stringify(list);
			console.log(str.data);
			res.send(str);
		})
	}).catch(function(error){
		console.log(('error <= productHistoryKeyPointList' + error.message).error);
		res.send(error.message);
	})	
}
exports.productHistoryKeyPointList = function(req, res){
	var productEPC = req.body.productEPC;
	productHistoryKeyPointDBFind({productEPC: productEPC})
	.then(function(_list){
		return paraInfoDBFind({}).then(function(_paraList){
				var list = _.map(_list, function(_historyPoint){
					var para = _.findWhere(_paraList, {paraID: _historyPoint.paraID});
					if(para != null){
						_historyPoint.paraName = para.paraName;
						_historyPoint.paraValue = para.paraValue;
					}
					return _historyPoint;
				});
				var str = JSON.stringify(list);
				console.log(str.data);
				res.send(str);
		})
	}).catch(function(error){
		console.log(('error <= productHistoryKeyPointList' + error.message).error);
		res.send(error.message);
	})
};
exports.addProductHistoryKeyPoint = function(req, res){
	var body = req.body;
	var productEPC = body.productEPC;
	var actionID = body.actionID;
	var paraID = body.paraID;

	_addProductHistoryKeyPoint(productEPC, actionID, paraID).then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log(('error <= addProductHistoryKeyPoint' + error.message).error);
		res.send(error.message);
	})
}
exports.removeProductHistoryKeyPoint = function(req, res){
	var productEPC = req.body.productEPC;
	productHistoryKeyPointDBRemove({productEPC: productEPC}).then(function(_number){
		if(_number <= 0){
			res.send('noData');
		}else{
			res.send('ok');
		}
	}).catch(function(error){
		console.log(('error <= removeParaInfo ' + error.message).error);
		res.send('error');
	})
}

function _addProductHistoryKeyPoint(_productEPC, _actionID, _paraID){
	var paraNew = new productHistoryKeyPoint(_productEPC, _actionID, _paraID);
	if(paraNew.tryValidate()){
		return productHistoryKeyPointDBFind({productEPC: paraNew.productEPC, timeStamp: paraNew.timeStamp})
		.then(function(_list){
			if(_.size(_list) > 0){
				return productHistoryKeyPointDBRemove({productEPC: paraNew.productEPC, timeStamp: paraNew.timeStamp})
				.then(function(_number){
					return productHistoryKeyPointDBInsert(paraNew);
				})
			}else{
				return productHistoryKeyPointDBInsert(paraNew);
			}
		})
	}else{
		return Q.fcall(function(){
			throw new  Error('paraError');	
		})
	}
}
exports.testAddProductHistoryKeyPoint = function(req, res){
	_addProductHistoryKeyPoint('000001', 'A00002', 'P00001')
	.then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log(('error <= testAddParaInfo' + error.message).error);
		res.send('error');
	})
}
//****************************************************************************
