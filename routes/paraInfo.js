
var timeFormater = require('./timeFormat').getCurrentTime;
var _                  = require('underscore');
var Q                  = require('q');



function paraInfo(_paraID, _paraName, _paraValue){
	this.paraID = _paraID;
	// this.timeStamp = timeFormater();
	this.paraName = _paraName;
	this.paraValue = _paraValue;
}
paraInfo.prototype.tryValidate = function() {
	if(this.paraID == null || this.paraID.length <=0 
		|| this.paraName == null || this.paraName.length <= 0 
		|| this.paraValue == null || this.paraValue.length <= 0){
		console.log(('paraInfo tryValidate error').error);
		return false;
	}else{
		return true;
	} 
};

exports.paraList = function(req, res){
	var paraID = req.body.paraID;
	paraInfoDBFind({paraID: paraID})
	.then(function(_list){
		var str = JSON.stringify(_list);
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log(('error <= paraList' + error.message).error);
		res.send(error.message);
	})
};
exports.addParaInfo = function(req, res){
	var body = req.body;
	var paraID = body.paraID;
	var paraName = body.paraName;
	var paraValue = body.paraValue;

	_addParaInfo(paraID, paraName, paraValue).then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log(('error <= addParaInfo' + error.message).error);
		res.send(error.message);
	})
}
exports.removeParaInfo = function(req, res){
	var paraID = req.body.paraID;
	paraInfoDBRemove({paraID: paraID}).then(function(_number){
		if(_number <= 0){
			res.send('noData');
		}else{
			res.send('ok');
		}
	}).catch(function(error){
		console.log(('error <= removeParaInfo ' + error.message).error);
		res.send('error');
	})
}
exports.testAddParaInfo = function(req, res){
	_addParaInfo('000001', 'temp', '12')
	.then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log(('error <= testAddParaInfo' + error.message).error);
		res.send('error');
	})
}
function _addParaInfo(_paraID, _paraName, _paraValue){
	var paraNew = new paraInfo(_paraID, _paraName, _paraValue);
	console.dir(paraNew);
	if(paraNew.tryValidate()){
		return paraInfoDBFind({paraID: paraNew.paraID}).then(function(_list){
			if(_.size(_list) > 0){
				return paraInfoDBRemove({paraID: paraNew.paraID})
				.then(function(_number){
					return paraInfoDBInsert(paraNew);
				})
			}else{
				return paraInfoDBInsert(paraNew);
			}
		})
	}else{
		return Q.fcall(function(){
			throw new  Error('paraError');	
		})
	}
}

//****************************************************************************
