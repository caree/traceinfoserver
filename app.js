
/**
 * Module dependencies.
 */

require('./config');
var colors = require('colors');

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});

var Q                  = require('q');
var Datastore          = require('nedb');


var actionDB = new Datastore({ filename: 'action.db', autoload: true });
actionDBFind = Q.nbind(actionDB.find, actionDB);
actionDBRemove = Q.nbind(actionDB.remove, actionDB);
actionDBInsert = Q.nbind(actionDB.insert, actionDB);

var paraInfoDB = new Datastore({ filename: 'para.db', autoload: true });
paraInfoDBFind = Q.nbind(paraInfoDB.find, paraInfoDB);
paraInfoDBRemove = Q.nbind(paraInfoDB.remove, paraInfoDB);
paraInfoDBInsert = Q.nbind(paraInfoDB.insert, paraInfoDB);

var productHistoryKeyPointDB = new Datastore({ filename: 'productHistoryKeyPoint.db', autoload: true });
productHistoryKeyPointDBFind = Q.nbind(productHistoryKeyPointDB.find, productHistoryKeyPointDB);
productHistoryKeyPointDBRemove = Q.nbind(productHistoryKeyPointDB.remove, productHistoryKeyPointDB);
productHistoryKeyPointDBInsert = Q.nbind(productHistoryKeyPointDB.insert, productHistoryKeyPointDB);


var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var actionInfo = require('./routes/actionInfo');
var paraInfo = require('./routes/paraInfo');
var productHistoryKeyPoint = require('./routes/productHistoryKeyPoint');

var app = express();

// all environments
app.set('port', process.env.PORT || httpListeningPort);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
app.get('/', routes.index);
app.get('/indexapi', routes.indexAPI);
app.get('/overview', routes.overview);


app.post('/actionList', actionInfo.actionList);
app.post('/addAction', actionInfo.addAction);
app.get('/testAddAction', actionInfo.testAddAction);
app.post('/removeAction', actionInfo.removeAction);

app.post('/paraList', paraInfo.paraList);
app.post('/addParaInfo', paraInfo.addParaInfo);
app.get('/testAddParaInfo', paraInfo.testAddParaInfo);
app.post('/removeParaInfo', paraInfo.removeParaInfo);

app.post('/productHistoryKeyPointList', productHistoryKeyPoint.productHistoryKeyPointList);
app.post('/addProductHistoryKeyPoint', productHistoryKeyPoint.addProductHistoryKeyPoint);
app.get('/testAddProductHistoryKeyPoint', productHistoryKeyPoint.testAddProductHistoryKeyPoint);
app.post('/removeProductHistoryKeyPoint', productHistoryKeyPoint.removeProductHistoryKeyPoint);

app.get('/productHistoryKeyPointListAll', productHistoryKeyPoint.productHistoryKeyPointListAll);

http.createServer(app).listen(app.get('port'), function(){
  console.log(('trace Info server listening on port ' + app.get('port')).info);
});
